package ma.octo.assignement.exceptions;

public class InsufficientAvailableBalanceException extends Exception {

  private static final long serialVersionUID = 1L;

  public InsufficientAvailableBalanceException() {
    super("Insufficient balance");
  }

  public InsufficientAvailableBalanceException(String message) {
    super(message);
  }
}
