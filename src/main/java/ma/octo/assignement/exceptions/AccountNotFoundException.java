package ma.octo.assignement.exceptions;

public class AccountNotFoundException extends Exception {

  private static final long serialVersionUID = 1L;

  public AccountNotFoundException() {
    super("Account Not Found");
  }

  public AccountNotFoundException(String message) {
    super(message);
  }
}
