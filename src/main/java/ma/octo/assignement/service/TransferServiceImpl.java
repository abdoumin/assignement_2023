package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.utils.Validator;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsufficientAvailableBalanceException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
@Service
public class TransferServiceImpl implements TransferService{

    @Autowired
    AuditService auditService;
    @Autowired
    TransferRepository transferRepository;
    @Autowired
    AccountRepository accountRepository;


    public TransferServiceImpl(AuditService auditService, TransferRepository transferRepository, AccountRepository accountRepository) {
        this.auditService = auditService;
        this.transferRepository = transferRepository;
        this.accountRepository = accountRepository;
    }
    @Override
    public List<Transfer> findAll() {
        return transferRepository.findAll();
    }

    @Override
    public void createTransfer(TransferDto transferDto) throws InsufficientAvailableBalanceException, AccountNotFoundException, TransactionException {
        Account senderAccount = accountRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Account recieverAccount = accountRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        Validator.validateTransfert(senderAccount,recieverAccount,transferDto);

        senderAccount.setSolde(senderAccount.getSolde().subtract(transferDto.getMontant()));

        recieverAccount.setSolde(new BigDecimal(recieverAccount.getSolde().intValue() + transferDto.getMontant().intValue()));

        Transfer transfer = TransferMapper.map(transferDto,recieverAccount,senderAccount);

        transferRepository.save(transfer);

        auditService.transfer(transferDto);
    }
}
