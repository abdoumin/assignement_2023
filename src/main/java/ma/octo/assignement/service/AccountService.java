package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface AccountService {

    List<Account> getAccounts();

    Account save(Account a);

    Account getAccount(long id);

    Account update(Account account) throws AccountNotFoundException;
}
