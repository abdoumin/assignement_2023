package ma.octo.assignement.service;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface DepositService {

    void createDeposit(DepositDto depositDto) throws AccountNotFoundException, TransactionException;

    List<Deposit> findAll();

    Deposit getDepositById(long id);
}
