package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public List<Account> getAccounts() {
        return accountRepository.findAll();
    }

    @Override
    public Account save(Account a) {
        return accountRepository.save(a);
    }

    @Override
    public Account getAccount(long id) {
        return accountRepository.findById(id).get();
    }

    @Override
    public Account update(Account account) throws AccountNotFoundException {
        Account a = null;

        if (account!=null && account.getId()!=0){
            a = accountRepository.getById(account.getId());

            if (a != null){
                a.setSolde(account.getSolde()!=null?account.getSolde():a.getSolde());
            }else {
                throw new AccountNotFoundException();
            }
        }else {
            throw new AccountNotFoundException();
        }

        return a;
    }
}
