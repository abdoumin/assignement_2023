package ma.octo.assignement.service;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsufficientAvailableBalanceException;
import ma.octo.assignement.exceptions.TransactionException;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface TransferService {

    List<Transfer> findAll();

    void createTransfer(TransferDto transferDto) throws InsufficientAvailableBalanceException, AccountNotFoundException, TransactionException;
}
