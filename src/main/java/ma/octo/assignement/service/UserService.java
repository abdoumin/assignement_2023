package ma.octo.assignement.service;

import ma.octo.assignement.domain.User;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface UserService {
    User addNewUser(User u);

    User loadUserByUsername(String username);

    List<User> listUsers();

}
