package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.utils.Validator;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.DepositRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DepositServiceImpl implements DepositService{

    @Autowired
    AuditService auditService;
    @Autowired
    DepositRepository depositRepository;
    @Autowired
    AccountRepository accountRepository;

    public DepositServiceImpl(AuditService auditService, DepositRepository depositRepository, AccountRepository accountRepository) {
        this.auditService = auditService;
        this.depositRepository = depositRepository;
        this.accountRepository = accountRepository;

    }
    @Override
    public void createDeposit(DepositDto depositDto) throws AccountNotFoundException, TransactionException {
        Account account = accountRepository.findByRib(depositDto.getCompteBeneficiaire().getRib());

        Validator.validateDeposit(account,depositDto);

        account.setSolde(account.getSolde().add(depositDto.getMontant()));

        Deposit deposit = DepositMapper.map(depositDto,account);

        depositRepository.save(deposit);

        auditService.deposit(depositDto);
    }

    @Override
    public List<Deposit> findAll() {
        return depositRepository.findAll();
    }

    @Override
    public Deposit getDepositById(long id) {
        return depositRepository.getById(id);
    }


}
