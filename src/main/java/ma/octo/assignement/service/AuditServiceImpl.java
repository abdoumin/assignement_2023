package ma.octo.assignement.service;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.utils.EventType;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.repository.AuditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditServiceImpl implements AuditService {

    Logger LOGGER = LoggerFactory.getLogger(AuditServiceImpl.class);

    @Autowired
    private AuditRepository auditTransferRepository;

    public void transfer(TransferDto transferDto) {

        LOGGER.info("Audit de l'événement {}", EventType.TRANSFER);

        Audit audit = new Audit();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage("Transfer from " + transferDto.getNrCompteEmetteur() + " to " + transferDto
                .getNrCompteBeneficiaire() + " for an amount of " + transferDto.getMontant()
                .toString());
        auditTransferRepository.save(audit);
    }


    public void deposit(DepositDto depositDto) {

        LOGGER.info("Audit de l'événement {}", EventType.DEPOSIT);

        Audit audit = new Audit();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage("Depot sur le compte " + depositDto.getCompteBeneficiaire().getNrCompte() + " par " + depositDto
                .getNomPrenomEmetteur() + " d'un montant de " + depositDto.getMontant()
                .toString());
        auditTransferRepository.save(audit);
    }
}
