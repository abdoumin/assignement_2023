package ma.octo.assignement.service;

import ma.octo.assignement.domain.User;
import ma.octo.assignement.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImp implements UserService{

    private UserRepository userRepository;
//    private PasswordEncoder passwordEncoder;

    public UserServiceImp(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User addNewUser(User u) {
        String pw = u.getPassword();
        u.setPassword(pw);
        return userRepository.save(u);
    }

    @Override
    public User loadUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> listUsers() {
        return userRepository.findAll();
    }
}
