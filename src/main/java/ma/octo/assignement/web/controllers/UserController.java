package ma.octo.assignement.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.service.AccountService;
import ma.octo.assignement.service.AccountServiceImpl;
import ma.octo.assignement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
public class UserController {
    @Autowired
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("users")
    List<User> appUsers(){
        return  userService.listUsers();
    }

    @PostMapping("users")
    User saveUser(@RequestBody User u){
        return userService.addNewUser(u);
    }



}
