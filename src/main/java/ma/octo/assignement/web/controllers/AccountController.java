package ma.octo.assignement.web.controllers;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/accounts/")
@RestController
public class AccountController {
    @Autowired
    AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("all")
    List<Account> getAccounts() {
        return accountService.getAccounts();
    }

    @GetMapping("{id}")
    Account getUsers(@PathVariable (name = "id") long id) {
        return accountService.getAccount(id);
    }


    @PostMapping("create")
    Account create(@RequestBody Account account){
        return accountService.save(account);
    }


    @PostMapping("update")
    Account update(@RequestBody Account account) throws AccountNotFoundException {
        return accountService.update(account);
    }
}
