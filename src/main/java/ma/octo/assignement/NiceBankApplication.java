package ma.octo.assignement;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.UserRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.AccountService;
import ma.octo.assignement.service.DepositService;
import ma.octo.assignement.service.TransferService;
import ma.octo.assignement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class NiceBankApplication implements CommandLineRunner {
	@Autowired
	private AccountRepository compteRepository;
	@Autowired
	private UserRepository utilisateurRepository;
	@Autowired
	private TransferRepository transferRepository;

	@Autowired
	private UserService userService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private TransferService transferService;
	@Autowired
	private DepositService depositService;

	public static void main(String[] args) {
		SpringApplication.run(NiceBankApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		User utilisateur1 = new User();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Male");
		utilisateur1.setPassword("password");

		userService.addNewUser(utilisateur1);


		User utilisateur2 = new User();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");
		utilisateur2.setPassword("password");

		userService.addNewUser(utilisateur2);

		Account compte1 = new Account();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUser(utilisateur1);

		accountService.save(compte1);

		Account compte2 = new Account();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUser(utilisateur2);

		accountService.save(compte2);

		Transfer v = new Transfer();
		v.setMontantTransfer(BigDecimal.valueOf(250L));
		v.setCompteBeneficiaire(compte2);
		v.setCompteEmetteur(compte1);
		v.setDateExecution(new Date());
		v.setMotifTransfer("Assignment 2021");

		transferService.createTransfer(TransferMapper.map(v));

		Deposit deposit = new Deposit();
		deposit.setMotifDeposit("Depot d'argent");
		deposit.setMontant(BigDecimal.valueOf(250L));
		deposit.setCompteBeneficiaire(compte1);
		deposit.setNomPrenomEmetteur("ayoub");
		deposit.setDateExecution(new Date());

		depositService.createDeposit(DepositMapper.map(deposit));
	}
}
