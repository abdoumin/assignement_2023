package ma.octo.assignement.utils;

import io.micrometer.core.instrument.util.StringUtils;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsufficientAvailableBalanceException;
import ma.octo.assignement.exceptions.TransactionException;

public class Validator {

    private static final int MAXIMUM_AMOUNT = 12000;
    private static final int MINIMUM_AMOUNT = 50;

    public static void validateTransfert(Account senderAccount, Account recieverAccount, TransferDto transferDto) throws InsufficientAvailableBalanceException,
            AccountNotFoundException, TransactionException {
        if (senderAccount == null) {
            throw new AccountNotFoundException("Emitter's account Non-existent");
        }

        if (recieverAccount == null) {
            throw new AccountNotFoundException("Receiver Account Non-existent");
        }

        if (transferDto.getMontant() == null || transferDto.getMontant().intValue() == 0) {
            throw new TransactionException("Empty amount");

        } else if (transferDto.getMontant().intValue() < MINIMUM_AMOUNT) {
                throw new TransactionException("Minimum transfer amount not reached");


        } else if (transferDto.getMontant().intValue() > MAXIMUM_AMOUNT) {
            throw new TransactionException("Maximum transfer amount exceeded");
        }

        if (transferDto.getMotif() == null || transferDto.getMotif().length() < 1) {
            throw new TransactionException("Blank motif");
        }

        if (senderAccount.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
            throw new InsufficientAvailableBalanceException();
        }
    }

    public static void validateDeposit(Account account, DepositDto depositDto) throws TransactionException, AccountNotFoundException {


        if (account == null) {
            throw new AccountNotFoundException();
        }
        if (depositDto.getMontant().intValue() > MAXIMUM_AMOUNT) {
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (StringUtils.isEmpty(depositDto.getNomPrenomEmetteur())) {
            throw new TransactionException("Nom du depositaire non existant");
        }

        if (StringUtils.isEmpty(depositDto.getMotif())) {
            throw new TransactionException("Motif vide");
        }
    }

}
