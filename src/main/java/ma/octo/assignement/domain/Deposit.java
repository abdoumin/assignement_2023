package ma.octo.assignement.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "DEP")
public class Deposit {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(precision = 16, scale = 2, nullable = false)
  private BigDecimal Montant;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateExecution;

  @Column
  private String nomPrenomEmetteur;

  @ManyToOne
  private Account compteBeneficiaire;

  @Column(length = 200)
  private String motifDeposit;

  public BigDecimal getMontant() {
    return Montant;
  }

  public void setMontant(BigDecimal montant) {
    this.Montant = montant;
  }

  public Date getDateExecution() {
    return dateExecution;
  }

  public void setDateExecution(Date dateExecution) {
    this.dateExecution = dateExecution;
  }

  public Account getCompteBeneficiaire() {
    return compteBeneficiaire;
  }

  public void setCompteBeneficiaire(Account compteBeneficiaire) {
    this.compteBeneficiaire = compteBeneficiaire;
  }

  public String getMotifDeposit() {
    return motifDeposit;
  }

  public void setMotifDeposit(String motifDeposit) {
    this.motifDeposit = motifDeposit;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNomPrenomEmetteur() {
    return nomPrenomEmetteur;
  }

  public void setNomPrenomEmetteur(String nomPrenomEmetteur) {
    this.nomPrenomEmetteur = nomPrenomEmetteur;
  }
}
