package ma.octo.assignement.dto;

import ma.octo.assignement.domain.Account;

import java.math.BigDecimal;
import java.util.Date;

public class DepositDto extends TransactionDto {

    private String nomPrenomEmetteur;

    private Account compteBeneficiaire;


    public String getNomPrenomEmetteur() {
        return nomPrenomEmetteur;
    }

    public void setNomPrenomEmetteur(String nomPrenomEmetteur) {
        this.nomPrenomEmetteur = nomPrenomEmetteur;
    }

    public Account getCompteBeneficiaire() {
        return compteBeneficiaire;
    }

    public void setCompteBeneficiaire(Account compteBeneficiaire) {
        this.compteBeneficiaire = compteBeneficiaire;
    }

}
