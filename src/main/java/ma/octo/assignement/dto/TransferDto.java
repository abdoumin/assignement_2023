package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

public class TransferDto extends TransactionDto {
  private String nrCompteEmetteur;
  private String nrCompteBeneficiaire;

  public String getNrCompteEmetteur() {
    return nrCompteEmetteur;
  }

  public void setNrCompteEmetteur(String nrCompteEmetteur) {
    this.nrCompteEmetteur = nrCompteEmetteur;
  }

  public String getNrCompteBeneficiaire() {
    return nrCompteBeneficiaire;
  }

  public void setNrCompteBeneficiaire(String nrCompteBeneficiaire) {
    this.nrCompteBeneficiaire = nrCompteBeneficiaire;
  }

}
