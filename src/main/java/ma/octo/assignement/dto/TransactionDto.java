package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

public class TransactionDto{

    private String motif;

    private BigDecimal montant;

    private Date date;

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
