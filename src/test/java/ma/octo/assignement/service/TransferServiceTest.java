package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsufficientAvailableBalanceException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransferSeviceTest {

    private Transfer transfer;
    private Account account1;
    private Account account2;

    private User appUser1;
    private User appUser2;


    @InjectMocks
    private TransferServiceImpl transferService;

    @Mock
    TransferRepository transferRepository;
    @Mock
    AuditService auditService;
    @Mock
    DepositRepository depositRepository;
    @Mock
    AccountRepository accountRepository;


    @BeforeEach
    void initialize(){
        appUser1 = new User();
        appUser1.setUsername("user1");
        appUser1.setLastname("last1");
        appUser1.setFirstname("first1");
        appUser1.setGender("Male");


        appUser2 = new User();
        appUser2.setUsername("user2");
        appUser2.setLastname("last2");
        appUser2.setFirstname("first2");
        appUser2.setGender("Female");


        account1 = new Account();
        account1.setId(1L);
        account1.setNrCompte("010000A000001000");
        account1.setRib("RIB1");
        account1.setSolde(BigDecimal.valueOf(20000));
        account1.setUser(appUser1);


        account2 = new Account();
        account2.setId(2L);
        account2.setNrCompte("010000B025001000");
        account2.setRib("RIB2");
        account2.setSolde(BigDecimal.valueOf(14000));
        account2.setUser(appUser2);


        transfer = new Transfer();
        transfer.setMotifTransfer("Transfer d'argent pour une urgence !");
        transfer.setMontantTransfer(BigDecimal.valueOf(2000));
        transfer.setCompteBeneficiaire(account2);
        transfer.setCompteEmetteur(account1);
        transfer.setDateExecution(new Date());

    }

    @AfterEach
    void destroy(){
        account1=null;
        account2=null;

        appUser1=null;
        appUser2=null;
    }

    @DisplayName("The sender accound has not been provided so an AccounfNotFoundException should be raised")
    @Test
    public void testTransferSenderAccountNotFoundException(){

        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(null);

        AccountNotFoundException accountNotFoundException = assertThrows(AccountNotFoundException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(accountNotFoundException.getMessage(),"Emitter's account Non-existent");

    }

    @DisplayName("The receiver accound is null so an AccoundNotFoundException should be raised")
    @Test
    public void testTransferRecieverAccountNotFoundException(){

        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(null);

        AccountNotFoundException accountNotFoundException = assertThrows(AccountNotFoundException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(accountNotFoundException.getMessage(),"Receiver Account Non-existent");

    }

    @DisplayName("The transaction money is null so a TransactionException should be raised")
    @Test
    public void testTransferZeroMoneyException(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        transfer.setMontantTransfer(BigDecimal.valueOf(0));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Empty amount");

    }

    @DisplayName("The transaction money is less than the minimum value so a TransactionException should be raised")
    @Test
    public void testTransferInsuffiscientMoneyException(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        transfer.setMontantTransfer(BigDecimal.valueOf(2));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Minimum transfer amount not reached");

    }

    @DisplayName("The transaction money is higher than the maximum barrier so a TransactionException should be raised")
    @Test
    public void testTransferMaximumMoneyReachedException(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        transfer.setMontantTransfer(BigDecimal.valueOf(2000000));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Maximum transfer amount exceeded");

    }


    @DisplayName("The sender's money is less than the transaction money so an InsufficientExcetpion should be raised")
    @Test
    public void testTransferInsuffisantBalanceException(){
        account1.setSolde(BigDecimal.valueOf(300));

        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        InsufficientAvailableBalanceException transactionException = assertThrows(InsufficientAvailableBalanceException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Insufficient balance");

    }


    @DisplayName("The transaction motif is not given so a TransactionException should be raised")
    @Test
    public void testTransferTransactionMotifException(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        transfer.setMotifTransfer(null);

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Blank motif");

    }


    @DisplayName("The amount transfered from the sender should be reduced from his account ")
    @Test
    public void testTransferHasBeenSuccessfull(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        try {
            BigDecimal expectedAccount1Sold = account1.getSolde().subtract(transfer.getMontantTransfer());
            BigDecimal expectedAccount2Sold =account2.getSolde().add(transfer.getMontantTransfer());
            transferService.createTransfer(TransferMapper.map(transfer));
            assertEquals(expectedAccount1Sold,account1.getSolde());
            assertEquals(expectedAccount2Sold,account2.getSolde());

        } catch (TransactionException e) {
            throw new RuntimeException(e);
        } catch (InsufficientAvailableBalanceException e) {
            throw new RuntimeException(e);
        } catch (AccountNotFoundException e) {
            throw new RuntimeException(e);
        }


    }



}