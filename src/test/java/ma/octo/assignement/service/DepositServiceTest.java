package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.DepositRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class DepositServiceTest {

    private Deposit deposit;
    private Account account1;
    private User appUser1;


    @InjectMocks
    private DepositServiceImpl depositService;

    @Mock
    AuditService auditService;
    @Mock
    DepositRepository depositRepository;
    @Mock
    AccountRepository accountRepository;



    @BeforeEach
    void initialize(){
        appUser1 = new User();
        appUser1.setUsername("user1");
        appUser1.setLastname("last1");
        appUser1.setFirstname("first1");
        appUser1.setGender("Male");

        account1 = new Account();
        account1.setId(1L);
        account1.setNrCompte("010000A000001000");
        account1.setRib("RIB1");
        account1.setSolde(BigDecimal.valueOf(200000L));
        account1.setUser(appUser1);


        deposit = new Deposit();
        deposit.setMotifDeposit("Depot d'argent pour urgence !");
        deposit.setMontant(BigDecimal.valueOf(2000L));
        deposit.setCompteBeneficiaire(account1);
        deposit.setNomPrenomEmetteur("Aboudou");
        deposit.setDateExecution(new Date());
    }

    @AfterEach
    void destroy(){
        account1=null;
        appUser1=null;
        deposit = null;
    }

    @Test
    public void getAllDepositsList()
    {
        given(depositRepository.findAll()).willReturn(List.of(deposit));

        // when -  action or the behaviour that we are going test
        List<Deposit> depositList = depositService.findAll();

        // then - verify the output
        assertThat(depositList).isNotNull();
        assertThat(depositList.size()).isEqualTo(1);
    }

    @DisplayName("No Account is given so an AccountNoFoundException should be raised")
    @Test
    public void testDepositAccountNotFoundException(){
        when(accountRepository.findByRib(any())).thenReturn(null);

        AccountNotFoundException accountNotFoundException = assertThrows(AccountNotFoundException.class,()->{
            depositService.createDeposit(DepositMapper.map(deposit));
        });

        assertEquals(accountNotFoundException.getMessage(),"Account Not Found");

    }

    @DisplayName("The transaction money is higher than the specified one so a TransactionException should be raised")
    @Test
    public void testDepositTransactionExceedTransactionMoneyException(){
        when(accountRepository.findByRib(any())).thenReturn(account1);
        deposit.setMontant(BigDecimal.valueOf(2000000L));
        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            depositService.createDeposit(DepositMapper.map(deposit));
        });

        assertEquals(transactionException.getMessage(),"Montant maximal de transfer dépassé");

    }


    @DisplayName("No Transmitter name  has been given so a TransactionException should be raised")
    @Test
    public void testDepositTransactionTransmitterException(){
        when(accountRepository.findByRib(any())).thenReturn(account1);

        deposit.setNomPrenomEmetteur("");

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            depositService.createDeposit(DepositMapper.map(deposit));
        });

        assertEquals(transactionException.getMessage(),"Nom du depositaire non existant");

    }

    @Test
    public void testDepositShouldIncreaseRecieverAmount() throws Exception{
        when(accountRepository.findByRib(any())).thenReturn(account1);

        BigDecimal account1ExpectedAmount = account1.getSolde().add(deposit.getMontant());

        depositService.createDeposit(DepositMapper.map(deposit));
        assertEquals(account1ExpectedAmount,deposit.getCompteBeneficiaire().getSolde());
    }






}
